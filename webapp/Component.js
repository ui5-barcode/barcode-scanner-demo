/*
 * (c) Copyright Tobias Hofmann
 * https://www.itsfullofstars.de
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */
sap.ui.define([
  "sap/ui/core/UIComponent",
  "sap/ui/Device",
  "de/itsfullofstars/BarcodeScannerDemo/model/models"
], function(UIComponent, Device, models) {
  "use strict";

  return UIComponent.extend("de.itsfullofstars.BarcodeScannerDemo.Component", {

    metadata: {
      manifest: "json"
    },

    /**
     * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
     * @public
     * @override
     */
    init: function() {
      // call the base component's init function
      UIComponent.prototype.init.apply(this, arguments);

      // enable routing
      this.getRouter().initialize();

      // set the device model
      this.setModel(models.createDeviceModel(), "device");
    }
  });
});
