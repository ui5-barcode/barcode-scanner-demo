# UI5 Barcode Scanner Demo

This is a demo UI5 app showing how to use the [custom UI5 control / library Barcode Scanner](https://gitlab.com/ui5-barcode/barcode-scanner). The app can be run using [UI5 tooling](https://sap.github.io/ui5-tooling/).

## Run app

Clone the repository and make sure you have ui5 tooling installed (npm install --global @ui5/cli).

```sh
npm install
ui5 serve
```

Access the app at http://localhost:8080

The app is configured to scan for EAN barcodes. In case you want to test the app with a different kind of barcode, like code 128, you have to change the source code of the home view. The control reads the expected code from property 

```json
code="ean_reader"
```

Here you can add the code you want to scan. It must be compatible by Quagga(2), like code123_reader.

A sample barcode can be obtained from the [Wikipedia about barcodes](https://en.wikipedia.org/wiki/Barcode).

![sample](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/UPC-A-036000291452.svg/1920px-UPC-A-036000291452.svg.png)

## Screenshots

Note that the camera shows brackets as well as marks the identified barcode with borders.

![demo1](./demo1.png)

![demo2](./demo2.png)
